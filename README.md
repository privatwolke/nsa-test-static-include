nsa-test-static-include
===========

This project tests the mobile NFCReader based, PC/SC card reader.
It includes the PCSC Reader jar statically as java provider of the current JDK.
This way the JAR file does not have be included into the project itself and the Provider does not have to be registered by the program manually.
It is assumed that our provider can be fetched via "getDefault()".

Installation instructions
===========

- Our Provider uses Log4J therefore your program has to configure Log4J before fetching the TerminalFactory.

        PropertyConfigurator.configure("log4j.properties");

- The generated JAR file "pcscReader.jar" from the project [nsa-desktop](https://github.com/ChristophPrybila/nsa-desktop) has to be installed into your JDK/JRE as an third Party extension. It will be considered an installed extension if it is placed in the standard place for the JAR files of an installed extension:

        $JAVA_HOME/jre/lib/ext           [Solaris]
        $JAVA_HOME\jre\lib\ext           [Windows]
        example:C:\jdk1.6.0\jre\lib\ext  

- Next you need to add it as an entry to the java.security file which can be found in $JAVA_HOME/jre/lib/security/java.security for the JRE/JDK you are using. Look for a list of lines with security.provider.X where X is some number. At the bottom of the list add the line:

        security.provider.N=at.ac.tuwien.mnsa.smartcard.MNSAProvider

- Note that we install a Provider of the type "PC/SC" as a TerminalFactory. If the program fetches the TerminalFactory with "getDeault()" it picks the registered TerminalFactory with the highest priority, so make sure that you insert the provider with the highest priority. 
Alternatively you can fetch the TerminalFactory directly with 

        TerminalFactory.getInstance("PC/SC", null, "MNSAProvider");

